A few terms that are used in this document wich may sound general but
have actually a very clear distinct technical meaning, thus they are
defined here for clarity.
**NOT** a dictionary (use Wikipedia).


# Resource

- distinct kind of thing (noun)
- *not* the "R" in "REST", but [REST is all about Resources](https://en.wikipedia.org/wiki/Representational_state_transfer#Software_architecture)
- it's also what ["MVC"](https://en.wikipedia.org/wiki/Model-View-Controller) and ["CRUD"](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) are about:
- most standardized part of the app
- "resourceful" routes
- "resourceful" model/view/controllers


# Polymorph

> "Apples and Oranges can not be compared"

A set is *polymorph* if it contains more than one kind of thing.

Try to avoid polymorph sets as much as possible, because great care has to be
taken with them. Normally easy things like sorting tend to get very hard.
