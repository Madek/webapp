**Looking for the User Manual?** see [**Madek Help** (Madek Hilfe) by ZHdK's Madek Support Team](https://wiki.zhdk.ch/madek-hilfe/doku.php)


All the (non-user) documentation is contained [in the repository, see on github](https://github.com/zhdk/madek/tree/madek-v3/doc) and has these main sections:

- [Manual](manual/index.md)
– main documentation, description of business logic, aimed at everyone working on the project.

- [Developers](development/index.md)
– For developers working on Madek

- Apart from the textual docs, there are also some [diagrams](https://github.com/zhdk/madek/tree/madek-v3/doc/diagrams) to look at.
