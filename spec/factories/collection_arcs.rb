FactoryGirl.define do
  factory :collection_media_entry_arc, class: Arcs::CollectionMediaEntryArc do
  end

  factory :collection_collection_arc, class: Arcs::CollectionCollectionArc do
  end

  factory :collection_filter_set_arc, class: Arcs::CollectionFilterSetArc do
  end
end
