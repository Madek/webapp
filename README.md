# Madek

Madek is a media archival, management and sharing system that can archive
video, audio and image files along with their metadata. The system crowdsources the chore of
retrieving and assigning metadata to the users themselves instead of putting all the burden
on the archival expert.


## Installation

The recommended way to install Madek is via the [Madek
Ansible-Setup Project](https://github.com/zhdk/madek-ansible-setup).


## Documentation

Browse the [doc](./doc) folder for documentation. 

The entities diagrams [current](http://rawgit.com/zhdk/madek/madek-v3/doc/database_and_entities/Schema.svg)
and [v3](http://rawgit.com/zhdk/madek/madek-v3/doc/database_and_entities/Schema_vision.svg) are notable. 

The old [Madek-Wiki](http://github.com/zhdk/madek/wiki) is outdated and
contains mostly wrong information. 


## State 

### `madek-v3` branch 

<a href="http://ci2.zhdk.ch/cider-ci/ui/public/madek/madek-v3/tests,code-check,coverage/summary.html">
  <img src="http://ci2.zhdk.ch/cider-ci/ui/public/madek/madek-v3/tests,code-check,coverage/summary.svg?respond_width_200">
  </img>
</a>

[Code Climate](https://codeclimate.com/github/zhdk/madek/compare/madek-v3)


#### `next` branch

<a href="http://ci2.zhdk.ch/cider-ci/ui/public/madek/next/tests,code-check,coverage/summary.html">
  <img src="http://ci2.zhdk.ch/cider-ci/ui/public/madek/next/tests,code-check,coverage/summary.svg?respond_width_200">
  </img>
</a>


[Code Climate](https://codeclimate.com/github/zhdk/madek/compare/next)


#### `master` branch

<a href="http://ci2.zhdk.ch/cider-ci/ui/public/madek/master/tests,code-check,coverage/summary.html">
  <img src="http://ci2.zhdk.ch/cider-ci/ui/public/madek/master/tests,code-check,coverage/summary.svg?respond_width_200">
  </img>
</a>


[![Code Climate](https://codeclimate.com/github/zhdk/madek/badges/gpa.svg)](https://codeclimate.com/github/zhdk/madek)




# Contributors

The following people have contributed to this project:

* Thomas Schank
* Franco Sellitto
* Sebastian Pape
* Richard Rasu
* Max Albrecht
* Ramón Cahenzli
* Andrea Singh
* Olivier Heitz
* Tomáš Pospíšek
* Susanne Schumacher


## Copyright and license

Madek is (C) Zürcher Hochschule der Künste (Zurich University of the Arts).

Madek is Free Software under the GNU General Public License (GPL) v3, see the included LICENSE file for license details.

Visit our main website at http://www.zhdk.ch and the IT center 
at http://itz.zhdk.ch

Some of our developers like to use Navicat to visualize and optimize our database
stuff. We thank PremiumSoft CyberTech Ltd. for granting us a free Navicat Premium
license to support development of our Free/Open Source Software project:


[![alt text](https://github.com/zhdk/madek/raw/master/doc/images/zhdk_logo.png "ZHdK logo")](http://www.zhdk.ch) ![alt text](https://github.com/zhdk/madek/raw/master/doc/images/navicat_logo.png "Navicat Premium Logo")
